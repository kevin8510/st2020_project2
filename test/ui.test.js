const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(title).toBe('Join');
    await browser.close();
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R4', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.screenshot({path: 'test/screenshots/welcome.png'});
    await browser.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.screenshot({path: 'test/screenshots/error.png'});
    await browser.close();
})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R6', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitFor(2000)
    let title = await page.$eval('#messages > li > div.message__body > p', (content) => content.innerHTML);
    expect(title).toBe('Hi John, Welcome to the chat app');
    await browser.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R8', 100);
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('R8');

    await browser.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R9', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});

    await page.waitForSelector('#users > ol > li');
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');

    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R10', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitForSelector('#users > ol > li:nth-child(1)');
    let member1 = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });


    const browser_2 = await puppeteer.launch({ headless: !openBrowser });
    const page_2 = await browser_2.newPage();
    await page_2.goto(url);
    await page_2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Andy', {delay: 100});
    await page_2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R10', {delay: 100});
    await page_2.keyboard.press('Enter', {delay: 100});
    await page_2.waitForSelector('#users > ol > li:nth-child(2)');
    let member2 = await page_2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });

    expect(member1).toBe('John');
    expect(member2).toBe('Andy');

    await browser.close();
    await browser_2.close();
})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R11', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    
    let title = await page.$eval('#message-form > button', (content) => content.innerHTML);
    expect(title).toBe('Send');
    await browser.close();
})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R12', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitFor(3000)

    await page.type('#message-form > input[type=text]', 'Hi', {delay: 100});
    const send_buttom = await page.$('#message-form > button')
    await send_buttom.click()
    await page.waitFor(1000)
    let message = await page.$eval('#messages > li:nth-child(2) > div.message__body > p', (content) => content.innerHTML);
    expect(message).toBe('Hi');
    await browser.close();
})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R13', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});

    const browser_2 = await puppeteer.launch({ headless: !openBrowser });
    const page_2 = await browser_2.newPage();
    await page_2.goto(url);
    await page_2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page_2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R13', {delay: 100});
    await page_2.keyboard.press('Enter', {delay: 100});
    await page_2.waitFor(2000)

    await page.type('#message-form > input[type=text]', 'Hi', {delay: 100});
    const send_buttom = await page.$('#message-form > button')
    await send_buttom.click()
    await page_2.type('#message-form > input[type=text]', 'Hello', {delay: 100});
    const send_buttom_2 = await page_2.$('#message-form > button')
    await send_buttom_2.click()

    let member_1 = await page_2.$eval('#messages > li:nth-child(2) > div.message__title > h4', (content) => content.innerHTML);
    expect(member_1).toBe('John');
    let message_1 = await page_2.$eval('#messages > li:nth-child(2) > div.message__body > p', (content) => content.innerHTML);
    expect(message_1).toBe('Hi');

    let member_2 = await page_2.$eval('#messages > li:nth-child(3) > div.message__title > h4', (content) => content.innerHTML);
    expect(member_2).toBe('Mike');
    let message_2 = await page_2.$eval('#messages > li:nth-child(3) > div.message__body > p', (content) => content.innerHTML);
    expect(message_2).toBe('Hello');

    await browser.close();
    await browser_2.close();
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R14', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    let title = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(title).toBe('Send location');
    await browser.close();
})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R15', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitFor(2000)
    const loc_buttom = await page.$('#send-location')
    await loc_buttom.click()
    await browser.close();
})
